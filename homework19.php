<div class="main">
    <form action="" class="form">
        <h3 class="title">PAYMENT DETAILS</h3>
            <input class="form__choice" placeholder="NAME ON CARD" list="NAME ON CARD" id="nameCard" >
            <datalist id="NAME ON CARD"> 
                <option class="form__choice_item" value="Visa">Visa</option>
                <option class="form__choice_item" value="Mastercard">Mastercard</option>
                <option class="form__choice_item" value="Mocnay">Mocnay</option>
            </datalist>
        <div class="box">
             <input class="form__input" placeholder="CARD NUMBER" type="number" name="CARD NUMBER" id="CARD NUMBER">
             <input class="form__input" placeholder="EXPIRY DATE" type="date" name="EXPIRY DATE" id="EXPIRY DATE">
             <input class="form__input" placeholder="CARD NUMBER" type="number" name="CARD NUMBER" id="CARD NUMBER" >
        </div>
        <h3 class="title">BILLING ADDRESS</h3>
        <div class="box">
            <input class="form__input" placeholder="NAME ON CARD" type="text" name="NAME ON CARD" id="NAME ON CARD">
            <input class="form__input" placeholder="CITY" type="text" name="CITY" id="CITY">
        </div>
        <div class="box">
            <select class="form__dropdown" placeholder="STATE PROVINCE" name="STATE PROVINCE" id="STATE PROVINCE">
                <option class="form__dropdown_item" value="Nord">Nord</option>
                <option class="form__dropdown_item" value="Center">Center</option>
                <option class="form__dropdown_item" value="South">South</option>
            </select>
            <input class="form__input" placeholder="ZIP CODE" type="text" name="ZIP CODE" id="ZIP CODE">
        </div>
        <button class="form__button" type="submit" name="submit">PAY $888</button>
    </form>
</div>

<style>
    *{
        margin: 0;
        padding: 0;
        border: none;
        box-sizing: border-box;
    }
    .main {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 100%;
    }

    .form {
        widht: 80%;
    }

    .title {
        font-size: 20px;
        margin: 30px 0 10px 0;
    }

    .form__choice {
        width: 100%;
        padding: 7px 0;
        font-size: 17px;
        border-bottom: 1px solid black;
    }

    .form__choice_item {
        font-size: 17px;
        padding: 7px 0;
    }

    .form__input {
        width: 100%;
        font-size: 17px;
        border-bottom: 1px solid black;
        padding-bottom: 6px;
        margin: 20px 0;
    }

    .form__dropdown {
        width: 100%;
        padding: 7px 0;
        font-size: 17px;
        border-bottom: 1px solid black;
    }

    .form__dropdown_item {
        padding: 7px 0;
        font-size: 17px;
    }

    .box {
        display: flex;
        justify-content: space-between;
        gap: 30px;
    }

    .form__button {
        display: block;
        font-size: 17px;
        color: white; 
        line-height: 150%;
        width: 300px;
        background-color: black;
        border-radius: 10%;
        margin: 20px 0 20px auto;
    }
</style>